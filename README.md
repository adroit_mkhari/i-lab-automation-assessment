# i-lab automation assessment


- Driver Automation Assessment

Excel Reader Web Driver

- [ ] https://gitlab.com/adroit_mkhari/i-lab-automation-assessment/-/blob/master/src/test/java/i_lab/assessment/driver/DriverAssessment.java

Database Example:

- [ ] https://gitlab.com/adroit_mkhari/i-lab-automation-assessment/-/blob/master/src/test/java/files/transform/DatabaseTransactorTests.java

Flat File Example:

- [ ] https://gitlab.com/adroit_mkhari/i-lab-automation-assessment/-/blob/master/src/test/java/files/transform/FlatFileToDataTableTests.java

- API Automation Assessment

DOG API Test:

- [ ] https://gitlab.com/adroit_mkhari/i-lab-automation-assessment/-/blob/master/src/test/java/api/i_lab/dog_api/DogAPITest.java

DOG API TEST FEATURE FILE:

- [ ] https://gitlab.com/adroit_mkhari/i-lab-automation-assessment/-/blob/master/src/test/java/api/i_lab/dog_api/features/dog_api.feature

DOG API TEST STEPS:
- [ ] https://gitlab.com/adroit_mkhari/i-lab-automation-assessment/-/blob/master/src/test/java/api/i_lab/dog_api/steps/DogAPITestSteps.java


SWAGGER API Test:

- [ ] https://gitlab.com/adroit_mkhari/i-lab-automation-assessment/-/blob/master/src/test/java/api/i_lab/swagger_api/SwaggerAPITest.java

SWAGGER API TEST FEATURE FILE:

- [ ] https://gitlab.com/adroit_mkhari/i-lab-automation-assessment/-/blob/master/src/test/java/api/i_lab/swagger_api/features/swagger_api.feature

SWAGGER API TEST STEPS:
- [ ] https://gitlab.com/adroit_mkhari/i-lab-automation-assessment/-/blob/master/src/test/java/api/i_lab/swagger_api/steps/SwaggerAPITestSteps.java
