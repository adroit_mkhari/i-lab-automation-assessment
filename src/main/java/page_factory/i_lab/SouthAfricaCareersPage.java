package page_factory.i_lab;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import selenium.web.driver.commons.SeleniumBasePage;

public class SouthAfricaCareersPage extends SeleniumBasePage {
    @FindBy(xpath = "/html/body/section/div[2]/div/div/div/div[3]/div[2]/div/div/h3")
    private WebElement currentOpenings;

    @FindBy(xpath = "/html/body/section/div[2]/div/div/div/div[3]/div[2]/div/div/div/div/div/div[1]/div[1]/div[2]/div[1]/a")
    private WebElement firstJob;

    public SouthAfricaCareersPage(WebDriver webDriver) {
        super(webDriver);
    }

    public void waitForVisibilityOfCurrentOpenings() {
        WebDriverWait webDriverWait = new WebDriverWait(getDriver(), 60);
        webDriverWait.until(ExpectedConditions.visibilityOf(currentOpenings));
    }

    public JobSpecificationPage clickOnFirstJob() {
        firstJob.click();
        return new JobSpecificationPage(getDriver());
    }
}
