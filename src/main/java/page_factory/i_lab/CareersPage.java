package page_factory.i_lab;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import selenium.web.driver.commons.SeleniumBasePage;

public class CareersPage extends SeleniumBasePage {
    @FindBy(xpath = "/html/body/section/div[2]/div/div/div/div[3]/div[2]/div/div/div[3]/div[2]/div/div/h4[3]")
    private WebElement joinOurTeam;

    @FindBy(linkText = "South Africa")
    private WebElement southAfrica;

    public CareersPage(WebDriver webDriver) {
        super(webDriver);
    }

    public void waitForVisibilityOfJoinOurTeam() {
        WebDriverWait webDriverWait = new WebDriverWait(getDriver(), 60);
        webDriverWait.until(ExpectedConditions.visibilityOf(joinOurTeam));
    }

    public SouthAfricaCareersPage clickSouthAfrica() {
        moveToWebElementAndClick(southAfrica);
        return new SouthAfricaCareersPage(getDriver());
    }
}
