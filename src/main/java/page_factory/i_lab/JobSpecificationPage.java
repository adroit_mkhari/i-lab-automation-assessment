package page_factory.i_lab;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import selenium.web.driver.commons.SeleniumBasePage;

public class JobSpecificationPage extends SeleniumBasePage {
    @FindBy(linkText = "Apply Online")
    private WebElement applyOnline;

    public JobSpecificationPage(WebDriver webDriver) {
        super(webDriver);
    }

    public void waitForVisibilityOfApplyOnline() {
        WebDriverWait webDriverWait = new WebDriverWait(getDriver(), 60);
        webDriverWait.until(ExpectedConditions.visibilityOf(applyOnline));
    }

    public JobApplicationForm clickOnApplyOnline() {
        moveToWebElementAndClick(applyOnline);
        return new JobApplicationForm(getDriver());
    }
}
