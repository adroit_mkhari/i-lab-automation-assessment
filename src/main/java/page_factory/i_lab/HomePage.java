package page_factory.i_lab;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import selenium.web.driver.commons.SeleniumBasePage;

public class HomePage extends SeleniumBasePage {
    @FindBy(linkText = "CAREERS")
    private WebElement careers;

    public HomePage(WebDriver webDriver) {
        super(webDriver);
    }

    public void waitForVisibilityOfCareers() {
        WebDriverWait webDriverWait = new WebDriverWait(getDriver(), 60);
        webDriverWait.until(ExpectedConditions.visibilityOf(careers));
    }

    public CareersPage clickOnCareers() {
        moveToWebElementAndClick(careers);
        return new CareersPage(getDriver());
    }
}
