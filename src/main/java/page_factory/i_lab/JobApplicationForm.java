package page_factory.i_lab;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import selenium.web.driver.commons.SeleniumBasePage;

import static files.Config.reportSourcePath;

public class JobApplicationForm extends SeleniumBasePage {
    @FindBy(id = "applicant_name")
    private WebElement applicantName;

    @FindBy(id = "email")
    private WebElement email;

    @FindBy(id = "phone")
    private WebElement phone;

    @FindBy(id = "message")
    private WebElement message;

    @FindBy(xpath = "/html/body/section[2]/div[2]/div/div/div/div/div[2]/div[2]/form/fieldset[1]/div[5]/div/div/div[3]/input")
    private WebElement uploadInput;

    @FindBy(id = "wpjb_submit")
    private WebElement submit;

    @FindBy(xpath = "//*[@id=\"wpjb-form-job-apply\"]/div")
    private WebElement errorMessage;

    @FindBy(xpath = "//*[@id=\"wpjb-apply-form\"]/fieldset[1]/div[5]/div/ul/li")
    private WebElement uploadErrorMessage;

    public JobApplicationForm(WebDriver webDriver) {
        super(webDriver);
    }

    public void waitForVisibilityOfApplicationForm() {
        WebDriverWait webDriverWait = new WebDriverWait(getDriver(), 60);
        webDriverWait.until(ExpectedConditions.visibilityOf(applicantName));
    }

    public void inputApplicantName(String nameOfApplicant) {
        applicantName.sendKeys(nameOfApplicant);
    }

    public void inputEmail(String emailAddress) {
        email.sendKeys(emailAddress);
    }

    public void inputPhone(String phoneNumber) {
        phone.sendKeys(phoneNumber);
    }

    public void inputWhy(String reason) {
        message.sendKeys(reason);
    }

    public void setBrowseFilePath(String filePath) throws InterruptedException {
        moveToWebElement(uploadInput);
        Thread.sleep(1000);
        uploadInput.sendKeys(filePath);
    }

    public void clickSendApplication() {
        moveToWebElementAndClick(submit);
    }

    public String getErrorMessage() {
        try {
            return errorMessage.getText();
        } catch (Exception e) {
            return "";
        }
    }

    public String getUploadErrorMessage() {
        try {
            String error = uploadErrorMessage.getText();
            moveToWebElement(uploadErrorMessage);
            Thread.sleep(1000);
            takeScreenshot(reportSourcePath + "/screenshots/I Lab Online Application/" + getDateTimeStamp() + "/Upload Error");
            return error;
        } catch (Exception e) {
            return "";
        }
    }
}
