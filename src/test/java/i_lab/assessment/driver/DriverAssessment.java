package i_lab.assessment.driver;

import common.testing.Scenario;
import common.testing.TestCase;
import common.testing.helpers.RandomNumbers;
import common.testing.helpers.RegularExpression;
import common.testing.reporting.ReportWriter;
import common.testing.reporting.TestResultReportFlag;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import page_factory.i_lab.*;
import selenium.web.driver.DriverManagerFactory;
import selenium.web.driver.DriverType;
import selenium.web.driver.managers.DriverManager;

import java.util.Arrays;
import java.util.regex.Matcher;

public class DriverAssessment {
    private static final String[] reportableFields = {"Test Case No", "Scenario Description", "Result", "Comment"};
    private static TestCase testCase;
    private static ReportWriter reportWriter = new ReportWriter(reportableFields);

    @BeforeClass
    public static void setup() {
        testCase = new TestCase("iLab Driver Test",
                "src\\test\\resources\\data_config.properties",
                "I_LAB_DRIVER_TEST_DATA_WORKBOOK",
                "I_LAB_DRIVER_TEST_DATA_SHEET",
                "Test Case No",
                "Test Case No,Scenario Description,Result,Comment");


        try {
            reportWriter.createReport("iLab Driver Test");
            testCase.loadDataExcel();
            testCase.loadTestCaseScenarios();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterMethod
    public void tearDown() {
        try {
            // reportWriter.saveReport();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public void cleanup() {
        try {
            reportWriter.saveReport();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @DataProvider(name = "Excel")
    public Object[][] getTestCaseScenarios() {
        try {
            if (testCase != null) {
                Object[][] scenarioDataProvider = testCase.getScenarioDataProvider();
                return scenarioDataProvider;
            } else {
                throw new Exception("No Test Cases Loaded. ie., Test Case Is Null.");
            }
        } catch (Exception e) {
            // TODO: Handle Exception
            e.printStackTrace();
        }
        return null;
    }

    @Test(dataProvider = "Excel")
    public void scenarioTests(Scenario scenario) {
        DriverManager driverManager = DriverManagerFactory.getDriverManager(DriverType.CHROME);
        WebDriver webDriver = driverManager.getWebDriver(false);
        webDriver.get("https://www.ilabquality.com/");

        try {
            String testCaseNo = scenario.getCellValue("Test Case No");
            String scenarioDescription = scenario.getCellValue("Scenario Description");
            String applicantName = scenario.getCellValue("Applicant Name");
            String email = scenario.getCellValue("Email");
            String phone = scenario.getCellValue("Phone");
            String reasonWhy = scenario.getCellValue("Reason Why");
            String uploadFilePath = scenario.getCellValue("Upload File Path");
            String expectedErrorMessage = scenario.getCellValue("Expected Error Message");
            String expectedUploadErrorMessage = scenario.getCellValue("Expected Upload Error Message");

            reportWriter.writeToReport(Arrays.asList(reportableFields).indexOf("Test Case No"), testCaseNo, TestResultReportFlag.DEFAULT);
            reportWriter.writeToReport(Arrays.asList(reportableFields).indexOf("Scenario Description"), scenarioDescription, TestResultReportFlag.DEFAULT);

            HomePage homePage = new HomePage(webDriver);
            homePage.switchToWindowAndMaximize(0);
            homePage.waitForVisibilityOfCareers();
            CareersPage careersPage = homePage.clickOnCareers();
            Thread.sleep(2000);
            careersPage.waitForVisibilityOfJoinOurTeam();
            SouthAfricaCareersPage southAfricaCareersPage = careersPage.clickSouthAfrica();
            Thread.sleep(2000);
            southAfricaCareersPage.waitForVisibilityOfCurrentOpenings();
            JobSpecificationPage jobSpecificationPage = southAfricaCareersPage.clickOnFirstJob();
            Thread.sleep(2000);
            jobSpecificationPage.waitForVisibilityOfApplyOnline();
            JobApplicationForm jobApplicationForm = jobSpecificationPage.clickOnApplyOnline();
            Thread.sleep(2000);
            jobApplicationForm.waitForVisibilityOfApplicationForm();

            if (!applicantName.isEmpty()) {
                jobApplicationForm.inputApplicantName(applicantName);
                Thread.sleep(1000);
            }

            if (!email.isEmpty()) {
                jobApplicationForm.inputEmail(email);
                Thread.sleep(1000);
            }

            if (phone.isEmpty() || phone.equalsIgnoreCase("random")) {
                StringBuilder randomPhoneNumberStringBuilder = new StringBuilder("0");
                int numberCount = 0;
                while (numberCount++ < 9) {
                    randomPhoneNumberStringBuilder.append(RandomNumbers.getRandomNumber(0, 9));
                }

                String randomPhoneNumber = randomPhoneNumberStringBuilder.toString();

                RegularExpression regularExpression = new RegularExpression("[0-9]{10}", randomPhoneNumber);
                Matcher matcher = regularExpression.runMatch();
                boolean matches = matcher.matches();
                Assert.assertTrue(matches);
                jobApplicationForm.inputPhone(randomPhoneNumber);
                Thread.sleep(1000);
            }

            if (!reasonWhy.isEmpty()) {
                jobApplicationForm.inputWhy(reasonWhy);
                Thread.sleep(1000);
            }

            if (!uploadFilePath.isEmpty()) {
                jobApplicationForm.setBrowseFilePath(uploadFilePath);
                Thread.sleep(1000);
            }

            jobApplicationForm.clickSendApplication();
            Thread.sleep(5000);

            if (!expectedErrorMessage.isEmpty()) {
                String errorMessage = jobApplicationForm.getErrorMessage();
                if (!errorMessage.equalsIgnoreCase(expectedErrorMessage)) {
                    throw new Exception("Expected Error Message Not Correct.");
                }
            }

            if (!expectedUploadErrorMessage.isEmpty()) {
                String uploadErrorMessage = jobApplicationForm.getUploadErrorMessage();
                if (!uploadErrorMessage.equalsIgnoreCase(expectedUploadErrorMessage)) {
                    throw new Exception("Expected Upload Error Message Not Correct.");
                }
            }

            reportWriter.writeToReport(Arrays.asList(reportableFields).indexOf("Result"), "Pass", TestResultReportFlag.SUCCESS);
            reportWriter.increamentReportRowIndex();
        } catch (Exception e) {
            reportWriter.writeToReport(Arrays.asList(reportableFields).indexOf("Result"), "Fail", TestResultReportFlag.DEFAULT);
            reportWriter.writeToReport(Arrays.asList(reportableFields).indexOf("Comment"), e.getMessage(), TestResultReportFlag.DEFAULT);
            reportWriter.increamentReportRowIndex();
        } finally {
            webDriver.quit();
        }
    }
}
