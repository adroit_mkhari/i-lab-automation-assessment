@SwaggerApiFeatureTestScenarios
Feature: Swagger Api Feature

  Scenario: Swagger Api Feature Test Scenario
    Given Swagger API Endpoints
    Then Retrieve all available pets and confirm that the name doggie with category id 12 is on the list
    Then Add a new pet with an auto generated name and status available - Confirm the new pet has been added
    Then Retrieve the created pet using the ID