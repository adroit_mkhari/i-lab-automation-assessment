package api.i_lab.swagger_api;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@SwaggerApiFeatureTestScenarios"},
        features = {"src/test/java/api/i_lab/swagger_api/features"},
        glue = {"api/i_lab/swagger_api/steps"},
        plugin = {"com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/api/i_lab/swagger_api/report.html"},
        monochrome = true
)

public class SwaggerAPITest {
}
